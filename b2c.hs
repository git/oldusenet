{- usenet b-news to more or less modern usenet converter -}

import Data.String.Utils
import Data.Char

import Msg
import Converter

main :: IO ()
main = massConvert parse b2c

b2c :: Converter
b2c = Just
	. dummyXref
	. postedDate
	. ensureSubject
	. ensurePath
	. titleSubject
	. formatMessageId
	. articleIdMessageId

{- Make sure a Xref header is available, making one up if necessary. -}
dummyXref :: Step
dummyXref m = ensureHeader m (Header "Xref") "dummy dummy:1"

{- Add a dummy subject if neccessary. -}
ensureSubject :: Step
ensureSubject m = ensureHeader m (Header "Subject") "-"

{- Path was sometimes missing, and instead From contained a bang path, so
 - if there's not Path, use From. -}
ensurePath :: Step
ensurePath m = ensureHeader m (Header "Path") $ getHeader m (Header "From")

{- Subject was sometimes called Title -}
titleSubject :: Step
titleSubject m = ensureHeader m (Header "Subject") $ getHeader m (Header "Title")

{- Try to make a Date header available, by copying the Posted header
 - if necessary. If neither exists, date parsing will later fail. -}
postedDate :: Step
postedDate m = ensureHeader m (Header "Date") $ getHeader m (Header "Posted")

{- Message-ID was earlier called Article-I.D. -}
articleIdMessageId :: Step
articleIdMessageId m = ensureHeader m mid $ getHeader m (Header "Article-I.D.")

{- Message-Id should have format <u@h> -}
formatMessageId :: Step
formatMessageId m
	| startswith "<" i && endswith ">" i = m
	| otherwise = munge $ formatMessageId' barei
	where
		i = strip $ getHeader m mid
		barei = filter (`notElem` "<>") i
		munge v = note "fixed Message-ID" $
			replaceHeader m mid $ "<"++v++">"

{- If it looks like "foo.125", convert to "125@foo.UUCP" -}
formatMessageId' :: String -> String
formatMessageId' [] = "dummy@dummy"
formatMessageId' s
	| '@' `elem` s = s
	| otherwise = case uucpStyle s [] of
		Just (host, num) -> num++"@"++host++".UUCP"
		Nothing -> s++"@dummy"

uucpStyle :: String -> String -> Maybe (String, String)
uucpStyle [] _ = Nothing
uucpStyle (x:xs) c
	| x == '.' && not (null xs) && all isDigit xs = Just (reverse c, xs)
	| otherwise = uucpStyle xs (x:c)

mid :: Header
mid = Header "Message-ID"
