cat > npois << '@@@'
>From cbosg!harpo!npois!jak Thu Jan  7 16:27:09 1982
Subject: netnews registration form for npois (finally)
Newsgroups: net.general

Systems Dept. (Neptune, NJ)

Name of contact person at site: Jim Kutsch
Electronic mail address of contact person: npois!jak
U.S. Mail address of contact person:
Room NP-2F-106
Bell Telephone Labs
Holmdel, NJ 07733
Phone number of contact person: (201) 922-7277

Systems with whom news articles are exchanged: harpo, houxi, eiss, and u1100s
(what kind of link, who the neighbor(s) are): all the above are uucp

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.

We are willing to exchange news with other BTL sites
over uucp or BLICN.
@@@
