cat > utah-gr << '@@@'
>From cbosg!harpo!utah-cs!thomas Thu Jan  7 15:13:22 1982
Subject: Announcing utah-gr
Newsgroups: net.general

	Name of site: utah-gr - University of Utah Graphics Lab
		Vax 750 running 4.1bsd
	What the site is all about: Graphics/Cagd research

	Name of contact person at site: Spencer Thomas
	Electronic mail address of contact person:
		decvax!harpo!utah-cs!utah-gr!thomas
		thomas@utah-20
	U.S. Mail address of contact person:
		Spencer Thomas
		Computer Science Dept.
		University of Utah
		Salt Lake City, Utah 84112
	Phone number of contact person:
		(801) 581-8800

	Systems with whom news articles are exchanged:
		Various local systems only.  We get our news via
		utah-cs.

	Willingness (or lack thereof) to connect to new sites that
	want to join usenet.  If you run uucp, tell if new sites can
	call you, if you will poll them, what your policy is.
	If on the arpanet, are you willing to forward news on to new
	sites by establishing an arpanet usenet connection?
		We prefer to funnel all news to/from utah through
		utah-cs.

	If you want to publish your uucp phone number, login, and password,
	include that info.
		We can accept dialups (no dial out unit), and would be
		willing to pay phone charges.  Contact Spencer Thomas.



@@@
