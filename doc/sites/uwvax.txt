cat > uwvax << '@@@'
>From solomon@uwisc Sun Dec  6 20:35:16 1981
Via: ucbvax
Date: 6 Dec 1981 08:53:13-CST
From: solomon at uwisc
To: cbosgd!mark@berkeley
Subject: USENET membership form
Status: R

Name of site:
	uwvax
What the site is all about:
	University of Wisconsin Computer Sciences Department research machine.
	This is a VAX 11/780 with about 4 MB memory, two RM03's, four
	Fuji Winchester disks with Systems Industry controller (on the SBI),
	TU45 tape and about 56 lines (dz's and dh-equivalents).  Also at this
	site are 4 VAX 11/750's, a PDP-11/70 and various smaller PDP-11's,
	all soon to be connected in a local network.  Uwvax is restricted
	to faculty and graduate-student research (no course work).
	Our interests here include distributed operating systems, database,
	program development systems, VSLI design, and image processing,
	as well as topics in numerical analysis and math programming
	(optimization), and theory of computing.  The 4.8M$ "Crystal" project,
	funded by NSF, will be building a multicomputer out of about 50-100
	32-bit computers in the "mini-VAX" class (actual processor not yet
	chosen).  The CSNET Public Host and Service Host (including the CSNET
	Name Server) are also at this site (two of the 750's).

Name of contact person at site:
	Marvin Solomon
Electronic mail address of contact person:
	ucbvax!uwvax!solomon
	solomon@uwisc
	solomon@csnet-sh
U.S. Mail address of contact person:
	Prof. Marvin Solomon
	Computer Sciences
	1210 W. Dayton St.
	Madison, WI  53706
Phone number of contact person:
	(608) 262-1204 (CS department number)
	(608) 263-2844 (office)

Systems with whom news articles are exchanged:
(what kind of link, who the neighbor(s) are):
	Currently, we are polled by ihps3 late at night.  Soon, we will
	install version B news and then switch to receiving news from ucb
	over ARPANET.  We have no dial-out capability

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.
If on the arpanet, are you willing to forward news on to new
sites by establishing an arpanet usenet connection?
	We are willing to do a limited amount of relaying to other ARPANET
	sites.

If you want to publish your uucp phone number, login, and password,
include that info.
@@@
