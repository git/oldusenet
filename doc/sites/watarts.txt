cat > watarts << '@@@'
>From cbosg!ihnss!ucbvax!decvax!watmath!watarts!eric Sat Jan  2 09:16:53 1982
Date-Sent: Sat Jan  2 01:33:50 1982
		Official watarts usenet form
Status: R

Name of site: watarts
	Arts Computing, University of Waterloo.
What the site is all about:
	IBM rje/rjo site, computer music,
	general timesharing, remote floppy disk for micros.
	We have an 11/34 with full memory, two RM03 compatible drives, KMC-11.
	We run V7 modified by Vrije to map out buffers and inode block numbers.

Name of contact person at site: Eric Gisin
Electronic mail address of contact person: watmath!watarts!eric
Postal Mail address of contact person:
	Arts Computing Office
	PAS Building
	University of Waterloo,
	Waterloo, Ontario, CANADA  N2L 3G1
Phone number of contact person: 519-885-1211 ext. 2597

Systems with whom news articles are exchanged:
	news recieved from watmath.
(what kind of link, who the neighbor(s) are):
	local net link to watmath.

We have no dialin or dialout hardware.







@@@
