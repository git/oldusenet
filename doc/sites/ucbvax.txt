cat > ucbvax << '@@@'
>From mark Sun Apr  5 00:04:09 1981




Name of site: ucbvax (Ernie CoVax)
What the site is all about:
	General purpose machine for Computer Science research
	in the C.S. Division at Berkeley.

Name of contact person at site: Matt Glickman
Electronic mail address of contact person:
	ucbvax!glickman, arpavax.glickman@berkeley
U.S. Mail address of contact person:
	Computer Systems Research Group
	4th floor, Evans Hall
	University of California
	Berkeley, California 94720
Phone number of contact person: (415) 642-7780

Systems with whom news articles are exchanged:
(what kind of link, who the neighbor(s) are):
	ucbarpa (Berknet link)
	decvax ucsfcgl menlo70 ihnss (uucp)

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.
If on the arpanet, are you willing to forward news on to new
sites by establishing an arpanet usenet connection?
	Must be cleared with Prof. Fateman.  A faculty sponsor here
	at Berkeley is required.  A research connection with Berkeley
	is generally also required.  We especially do not want to be
	used as a communications gateway for other sites.
	Prof. Fateman's phone number is (415) 642-1879.

If you want to publish your uucp phone number, login, and password,
include that info.
	No.
@@@
