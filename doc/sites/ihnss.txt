cat > ihnss << '@@@'
>From cbosg!ihnss!karn Thu Dec 31 17:38:04 1981
Date-Sent: Thu Dec 31 13:38:21 1981 CST
To: cbosg!mark
Subject: usenet directory entry for ihnss
Status: R

	Name of site:

ihnss - Indian Hill New Switching Services

	What the site is all about:

Applied research and exploratory development of possible Bell System
switching services.  Located at Bell Telephone Labs, Naperville,
Illinois.

	Name of contact person at site:

Phil Karn

	Electronic mail address of contact person:

ihnss!karn (known from ucbvax)

	U.S. Mail address of contact person:

Phil Karn
Room 6E-326
Bell Telephone Labs
Naperville & Warrenville Rds.
Naperville, Il  60566

	Phone number of contact person:

312-979-4083

	Systems with whom news articles are exchanged:
	(what kind of link, who the neighbor(s) are):

uucp connections:
ucbvax	mhtsa	eagle	vax135	cbosg
All but ucbvax are Bell Labs sites. mhtsa and eagle are at Murray
Hill, New Jersey; vax135 is at Holmdel, New Jersey, and cbosg is
in Columbus, Ohio.

usend connections (uses the Bell Labs IBM remote job entry net):
ihuxf	ihuxg	ihuxh	ihuxi	ihuxj	ihuxk	ihuxl	ihuxm
ihuxn	ihuxo	ihuxp	ihuxs	ihps3	ih1ap	ihima	iwlc8
ihldt	ihlpb	houxi	druxj
We are the "central node" for netnews at Indian Hill. We also
forward to systems at other BTL locations.  All systems with the
"ih" prefix are here at Indian Hill; iwlc8 is Indian Hill West; houxi is
at Holmdel, New Jersey, and druxj is at Denver, Colorado.

In addition to these netnews sites, we have 17 "mail drop"
destinations for users on systems without netnews who wish to
receive specific newsgroups (e.g., fa.human-nets)

	Willingness (or lack thereof) to connect to new sites that
	want to join usenet.  If you run uucp, tell if new sites can
	call you, if you will poll them, what your policy is.
	If on the arpanet, are you willing to forward news on to new
	sites by establishing an arpanet usenet connection?

As you can see, we probably have some kind of record for the number
of sites that we feed.  I think we'd prefer not to send netnews
to any more sites via uucp.

	If you want to publish your uucp phone number, login, and password,
	include that info.

Send mail to ihnss!karn, and I'd be happy to exchange that
information in return for 2-way access.
@@@
