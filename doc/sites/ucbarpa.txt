cat > ucbarpa << '@@@'
>From mark Sun Apr  5 00:04:09 1981
Subject: usenet map

System name: ucbarpa
Path:	ucbvax!arpavax:user
Contact:Mark Horton (ucbvax!mark, csvax.mark@berkeley) (415) 642-4948

Willingness to call new sites:
	No policy exists yet.  A 300 baud dialer exists.  Most uucp
	traffic is currently routed through ucbvax.
Willingness to be called:
	No policy yet.  Uucp traffic routed through ucbvax.

.sys file entries: ucbvax
	ucbarpa is hooked into usenet over a berknet link

Purpose of machine:  Development machine for the Arpa Project at
Berkeley.  Support of Berkeley Vax Unix.

@@@
