cat > watmath << '@@@'
>From cbosg!decvax!watmath!dmmartindale Sun Dec 13 05:42:03 1981
Date-Sent: Sun Dec 13 04:00:19 1981
To: decvax!ucbvax!glickman mark shannon
Subject: USENET form
Cc: arwhite bstempelton dmmartindale watarts!spoon
Status: R


Name of site: watmath
What the site is all about:
	Primarily a teaching machine for operating systems, graphics, and
	compiler construction courses.  Secondary purpose is research:
	currently symbolic computation (vaxima, maple), language
	development (prolog, lucid), and a bit of Telidon.

Name of contact person at site: Dave Martindale
Electronic mail address of contact person: decvax!watmath!dmmartindale
U.S. Mail address of contact person:
	Dave Martindale
	Math Faculty Computing Facility
	University of Waterloo
	Waterloo, Ontario
	Canada.  N2L 3G1

Phone number of contact person: 519-885-1211 Ext 2684 (if no answer, leave
					message at Ext 3546)

Systems with whom news articles are exchanged:
	decvax (uucp, decvax usually polls us)
	watarts (uucp through Sytek local area net and Gandalf PACX)
	waterloo computer graphics laboratory (uucp name
		not yet determined) (uucp on hard line)
	We run B news.

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.
If on the arpanet, are you willing to forward news on to new
sites by establishing an arpanet usenet connection?
	Currently willing to let anyone talk to us; we have no dialer,
	so you'll have to phone us.  Number is 519-884-4109.
	Modem is Vadic 3451 triple modem, so 300 or 1200 is OK.
	Login uucp; no password (this may change in the future)
	Also beware that our network location may change in the
	near future.




@@@
