cat > ihldt << '@@@'
>From cbosg!ihnss!ihldt!rfs Fri Jan  1 16:38:05 1982
Date-Sent: Fri Jan  1 15:06:02 1982
To: ihnss!cbosg!cbosgd!mark
Subject: USENET directory

Name of site: ihldt
What the site is all about: Packet Switching

Name of contact person at site: R. F. Stork (Ron)
Electronic mail address of contact person: ihldt!rfs
U.S. Mail address of contact person: Bell Labs Indian Hill RM, 6C-302
Phone number of contact person: 312-979-2454

Systems with whom news articles are exchanged:
(what kind of link, who the neighbor(s) are): ihnss  via RJE link

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.
	I would consider forwarding to other RJE linked systems.
	While I have UUCP links, they are on dz-11 not running
	on KMC11 (I already run 64 9600 baud lines on KMC's) so
	I would not be willing to do much over UUCP.

SITE info:
		VAX 11/780
		4 MB RAM
		5 RP06
		RJE, PCL and UUCP links
		50 users

@@@
