cat > hao << '@@@'
>From menlo70!hao!pag  Thu Feb 26 14:35:49 1981
To: menlo70!ucbvax!mark
Subject: USENET map
Status: R

system name:	hao
path from:	ucbvax
full path from:	menlo70!ucbvax
contact:	peter gross
		(303)-494-5151 ext. 348
		FTS 322-5348
		login name: pag
uucp login:	uucp
uucp passwd:	uucp
dialup #:	(303)-494-0930  (300 or 1200 baud)
.sys sites:	menlo70
netnews users:	~10 (but we just started a few days ago)

We are the High Altitude Observatory, a division of the National Center for
Atmospheric Research (NCAR).  Most of our work is in Solar Astrophysics
research, with RJE to the NCAR CRAY-1 (soon to be a network).  We do not
have an auto-dialer, but other sites may call us at any time.

>From ucbvax!menlo70!hao!pag Wed Jan 13 16:37:17 1982
Date: 13-Jan-82 12:41:18-EST (Wed)
From: ucbvax!menlo70!hao!pag
Subject: hao usenet dir
Via: cbosgd.uucp (V3.73 [1/5/82]); 13-Jan-82 16:37:16-EST (Wed)
Mail-From: ucbvax received by cbosgd at 13-Jan-82 16:37:15-EST (Wed)
To: menlo70!ucbvax!cbosgd!mark
Status: R

HAO is a division of NCAR (National Center for Atmospheric Research)
in Boulder, CO.  US Mail addr:
    HAO/NCAR
    PO Box 3000
    Boulder CO  80307




@@@
