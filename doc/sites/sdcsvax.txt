cat > sdcsvax << '@@@'
>From ucbvax!sdcsvax!phil Thu Jan  7 21:22:54 1982
Date-Sent: Thu Jan  7 18:19:07 1982
To: ucbvax!mark
Subject: usenet form for sdcsvax
Status: R

Name of site: sdcsvax

What the site is all about:

	University of California, San Diego
	Deptartment of Electrical Engineering and Computer Sciences

Name of contact person at site: Phil Cohen

Electronic mail address of contact person:
	uucp: ucbvax!sdcsvax!phil
	arpa: sdcsvax!phil@nprdc
	(ucbvax!phil and phil@nprdc work too)

U.S. Mail address of contact person:
	ucsd
	eecs dept.
	mail code c-014
	la jolla, ca.   92093

Phone number of contact person: (714) 452-4708

Systems with whom news articles are exchanged:
	sdcattb  dataswitch
	sdcatta  dataswitch
	sdqmlab  dialer
	phonlab  dataswitch
	sdaaron  dialer
	dcdwest  dialer

(what kind of link, who the neighbor(s) are):
	local camups port selector (dataswitch) used for uucp
	connections also auto-dialer and modems.  Direct line to nprdc
	running uucp. (nprdc not running news)

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.
If on the arpanet, are you willing to forward news on to new
sites by establishing an arpanet usenet connection?

	Other sites can call us but we are out of long distance $$.
	Can make local calls, but not really willing to handle much
	more news traffic.

If you want to publish your uucp phone number, login, and password,
include that info.

	Not at this time...  Call or send mail if interested.
@@@
