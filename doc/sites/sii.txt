cat > sii << '@@@'
>From ucbvax!decvax!ittvax!sii!meg Sat Jan  9 11:24:51 1982
Subject: sii site announcement
Newsgroups: net.general

Name of site: 	sii
What the site is all about:

	Helping people migrate to Unix, including language processors
	and internal system work.  We also have several small Unix
	software products.

	We're an Onyx site; we'd like to know of others.

Name of contact people at site:
Electronic mail address of contact people:

	Marta Greenberg		(decvax!ittvax!sii!meg)
	Bill Ezell		(decvax!ittvax!sii!wje)
	David Dick		(decvax!ittvax!sii!drd)

U.S. Mail address of contact people:
Phone number of contact person:

	Software Innovations, Inc.
	440 Amherst Street
	Nashua, New Hampshire  03063

	(603) 883-9300

	Lat:  42:47' N
	Long: 71:31' W

Systems with whom news articles are exchanged:
(what kind of link, who the neighbor(s) are):

	ittvax (via uucp)

Willingness (or lack thereof) to connect to new sites that
want to join usenet.  If you run uucp, tell if new sites can
call you, if you will poll them, what your policy is.

	We have no auto-dialer, so we would have to be polled
	for news.  Since we have only a single modem which is
	heavily used for data transfers, we can't have people
	polling us often during business hours.  Polling at
	night is fine with us; we'll pass through all news we
	get.
@@@
