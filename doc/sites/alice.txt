cat > alice << '@@@'
>From cbosg!alice!ark Tue Nov 24 11:55:47 1981
Date-Sent: Sun Nov 22 23:53 EST 1981
Status: R

Name of site: alice
What the site is all about: This machine is used by people at Bell Labs
	doing research on various aspects of communications:
	computer science, psychology, acoustics, economics, mathematics.

Name of contact person at site: Andrew Koenig, alice!ark

		Andrew Koenig
		Room 2C-570
		Bell Laboratories
		Murray Hill NJ 07974

				(201) 582-5570

Systems with whom articles are exchanged (what kind of link,
who the neighbor(s) are): The major ones are allegra, research, and
mhtsa.  The link is a locally developed network that runs at
more than 1000 characters per second, except to mhtsa, which is
by 1200 baud dialup.

Willingness (or lack thereof) to connect to new sites that
want to join usenet:  I do not think we can afford to distribute
messages on a wholesale basis right now, but we will consider
individual requests from system administrators.  We are willing to
exchange uucp messages with other systems that have their own ACU,
but are reluctant to poll others, simply because of the number of
requests and the fact that our dialers are heavily loaded as it is.

uucp login information:
	System name: alice
	Machine particulars: VAX-11/780 running virtual UNIX/32V
	Access: 1200 or 300 baud dialup, through Bell 212A modems.
		Normal mode is 1200 baud on all lines.  A break
		will toggle between 1200 and 300.
	Phone number: 201-582-9800*0000*4575
		A * in this context means "wait for another dial tone"
		You must have TOUCH-TONE (R) service to reach this machine.
	Login: nuucp		Password: carroll



@@@
