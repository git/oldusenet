Back in 1994 when I was in college, I remember seeing versions of
"[Emily Postnews Answers Your Questions on Netiquette](http://www.templetons.com/brad/emily.html)",
and as I recall I wrote a paper for a Sociology class citing
it as an example of ways Usenet maintained its culture.
The irony of course, was that this was already in year two of
[The September that Never Ended](https://en.wikipedia.org/wiki/Eternal_September).

But back then, I was missing another important piece of the puzzle that
I stumbled over today in my buldging olduse.net feed, an article titled
"[Emily Post on Usenet Etiquette](http://article.olduse.net/642@eagle.UUCP)"

This "Emily Post" well predates Brad Templeton's better known "Emily Postnews",
and it's obvious that the latter was written as a parody of it.

Take this question in "Emily Post":

<pre>
        - "Where does 'fubar' come from?"

          In my opinion the best answer seems to be "Fouled up beyond
          all recognition." There are lots of versions of this
          acronym, in particular "Fouled" is usually replaced by a
          less polite word.  "foobar", "foo" and "bar" are all
          derived from "fubar."  (See discussion of net.jokes for the
          reason I use the polite word.)
</pre>

Which inspired this snark in "Emily Postnews":

<pre>
Q: What does foobar stand for?
A: It stands for you, dear.
</pre>

This difference in tone between the formal, clear, and staid "Emily Post",
and the informal, referential, and ironic "Emily Postnews" is one of the
things I'm most interested in seeing evolve as Usenet grows its own culture.

And so I was most interested to see 
[this response](http://article.olduse.net/516@ixn5c.UUCP) to "Emily Post",
which articulates the problem Usenet is struggling with in 1982 so well:

<blockquote>
Here we have a new media which is not quite a publication (such as
a memo, book or newspaper), not quite a conversation (there is a
definite time lag between messages), and not quite a "semi-public
forum" as some people would have you believe. 

The one criticism of the paper is that it implied to me that it was
socially unacceptable to use the net conversationally [...]
</blockquote>

[[!meta author=Joey]]
