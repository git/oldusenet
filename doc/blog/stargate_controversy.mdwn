Reading along in my favorite newsgroup, I saw "WARNING" and unexpectedly
landed in a controversy about Censorship from Spaaace!

	    Primarily, the message is  that,  unless  something  is
	done  shortly,  this  newsgroup  may not exist a year or two
	from now. [...]
	
	    A few months ago, a group of network administrators got
	together  and  decided  to fund a project called "STARGATE".
	Basicly, this was a  sound  idea.   News  sources  would  be
	routed to the STARGATE transmitter which would beam the mes-
	sages to a satellite which would, in turn,  relay  the  mes-
	sages  to  more localized network hubs, thus alleviating the
	need for as many long distance calls.  [...]
	
	Unfortunately, the people who have promoted this scheme
	could  not  leave  well  enough  alone.   They felt that the
	volume of "garbage" flowing through the net  was  too  high.
	They  felt  that the carrier of these messages might be able
	to be sued for possibly libelous messages.  They  felt  that
	this  was  their  chance  to  play God and they took it.  In
	short, the new network will have no unmoderated news. [...]
	
	Let the people who conceived of this know that it
	is  not  appreciated.   E-Mail  bomb them.  Flame them until
	they drop.  If you see them in public, spit on  them. [...]

-- [Frank Adrian](http://article.olduse.net/387@hercules.UUCP)

Wow! Digging into net.stargate, I see that Stargate plans
to bounce the traffic off the [Galaxy I](https://en.wikipedia.org/wiki/Galaxy_1)
satellite, hidden in the vertical interval of a TV station. Some more detail
[here](http://web.archive.org/web/19981203103811/www.stargate.com/history.html).

This proposed change is not being received well by the net at large.

	I agree that lots of Usenet submissions are trash.  Everything is trash
	to somebody.  What I don't want is centralized control.  That's the ONE
	thing that makes Usenet different from every other network, and it's a
	valuable difference.

-- [John Gilmore](http://article.olduse.net/1942@sun.uucp)

Some of the responses from Usenet admins are also very interesting,
as they provide a glimpse into what's involved in keeping the current network
running, and continuing to scale up.

	Because we have to deal with the net on a day to day (sometimes hour
	to hour basis) and talk to a LOT of the people who run the net, I think we
	have a different view of the net than the people who simply have the
	priviledge of using the damn thing. [...]
	
	I mean, seriously-- if you haven't
	spent months bickering with the carriers lawyers, how in the HELL can you
	suggest seriously that we really don't need moderators or that we can do it
	in software?
	
	If you people seriously want to try to get along without a group of
	overworked and harried people who happen to spend a lot of time keeping
	this thing running so you can bitch at us about it, then fine. Let me know.
	I'll rmgroup mod.singles, I'll unsubscribe to net.news, and I'll laugh if
	and when the system dies. We can cancel Stargate, we can watch the
	backbones start restricting news, the software can get flakey, and, if you
	could get it through the garbage, everyone who is yelling at the fascists
	for trying to do a thankless job would spend just as much time yelling at
	us for letting it fall apart. I don't know about the rest of them, but I
	am sometimes tempted to just step back and watch the damn thing die.

-- [Chuqui Q. Koala](http://article.olduse.net/2226@nsc.UUCP)

	This is another vote in favour of some source of moderated news -
	either via stargate or some other means.  While at Waterloo, I've
	watched the information flow on USENET grow from under 1Mb/week to its
	current level of over 4Mb/week.  I used to read almost everything that
	came in (except info-cpm, remember that?), now I read only a small
	fraction, and the valuable stuff is getting scarcer.

-- [Dave Martindale](http://article.olduse.net/963@watcgl.UUCP)
