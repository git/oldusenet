[A Letter from Xanadu](http://article.olduse.net/691@allegra.UUCP)
is an interesting look inside [Ted Nelson's Xandu project](http://en.wikipedia.org/wiki/Project_Xanadu).

<blockquote>
With the Sun workstation's bit map graphics capability, we can now start
working out he best way to visually display links.
</blockquote>

That sounds almost like it's 1992 and the web is being built.. but no, it's
a decade earlier.

<blockquote>
"Three-sets", the part
of the system which allows a programmer to include information about a link
other than where it connects to, are now implemented.
</blockquote>

And, of course, tried to do things that are still not available on the web
today.

<blockquote>
We recently fed a document of .75 megabytes into the system.  Linkages
within this document and from others have been made and tested; this is
demonstrable proof that the Xanadu Hypertext System can handle large
databases without degrading response time.
</blockquote>

Ah, it's still 1982 after all!

[[!meta author=Joey]]
