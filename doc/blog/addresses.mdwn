[[!meta title="Baroque (and unrepliable) addresses"]]

Pavel Curtis is a familiar name to me.. he wrote LambdaMOO!
But here, he's just 
[annoyed at email addresses](http://article.olduse.net/2629@Acornell.UUCP):

> I attempted to reply to a news item recently for which the return address
> was as follows:
>         vax135!harpo!mhtsa!ihnss!ucbvax!C70:sri-unix!KING@KESTREL
> I have now received my letter back from
>         vax135!harpo!mhtsa!ihnss!ucbvax!Network:c70
> telling me that there was "No such mailbox at this site"
> 
> Could someone please tell me just what the address means and how it gets
> generated and how I'm supposed to reply to it?

The [explanation was complicated](http://article.olduse.net/156@Adopey.UUCP);
the address tried to traverse both Usenet and the ARPANET, and failed.
Even parsing it was ambiguous.

Following up to that was this [awesome post](http://article.olduse.net/2127@Acbosgd.UUCP):

> So you want a reply command that works 100% of the time?  So does the
> rest of the world!  The UUCP/Berknet/ARPA environment is just too weird
> and full of glitches for any static program to handle it.  Berkeley's
> Mail program makes a valiant attempt but botches about half the time
> I try to use the reply command.

End-to-end communication that works all the time. Yes, that would be nice. :)

While I've read repeatedly how Sendmail's baroque syntax was designed
to allow for translation of email addresses between networks, I didn't
really understand the motivation until I saw this thread play out on
olduse.net. Was this thread perhaps an inspiration for sendmail?
It will hit the olduse.net sometime this year.

[[!meta author=Joey]]
