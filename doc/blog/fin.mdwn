Welcome back to the end of Usenet.

This exhibit began [10 years ago](http://joeyh.name/blog/entry/announcing_olduse.net/)
in 1981, spooling out Usenet history in real 
time with a 30 year delay. My archive has reached its end, and ten years is
more than long enough to keep running something you cobbled together
overnight way back when. So, this is the end for olduse.net.

The site will continue running for another week or so, to give you time to
read the last postings. Find the very last one, if you can!

The source code used to run it, and the content of this website have
themselves been archived up for posterity at
[The Internet Archive](https://archive.org/details/olduse.net).

Sometime in 2022, a spammer will purchase the domain, but not find it to be
of much value, because you're deleting this from your RSS reader now.

The Utzoo archives that underlay this have currently sadly
been [censored off the Internet](https://archive.org/details/utzoo-wiseman-usenet-archive)
by someone. This will be unsuccessful; by now they have spread and many
copies will live on.

----

But before I go, I told a lie ten years ago.

> You can post to olduse.net, but it won't show up for at least 30 years.

Actually, those posts drop *right now*! Here are the followups
to 30-year-old Usenet posts that I've accumulated over the past decade.

Mike replied in 2011 to JPM's post in 1981 on fa.arms-d
["Re: CBS Reports"](https://olduse.net/replies/14046-1307820987-1)

> A greeting from the future:
> I actually watched this yesterday (2011-06-10) after reading about it here.

Christian Brandt replied in 2011 to schrieb phyllis's post in 1981 on the "comments" newsgroup
["Re: thank you rrg"](https://olduse.net/replies/560-1307400023-1)

>  Funny, it will be four years until you post the first subnet post i
> ever read and another eight years until my own first subnet post shows up.

Bernard Peek replied in 2012 to mark's post in 1982 on net.sf-lovers
["Re: luke - vader relationship"](https://olduse.net/replies/7680-1353081388-1)

> > i suggest that darth vader is luke skywalker's mother.
> 
> You may be on to something there.

Martijn Dekker replied in 2012 to henry's post in 1982 on the "test" newsgroup
["Re: another boring test message"](https://olduse.net/replies/19312-1335487318-1)

trentbuck replied in 2012 to dwl's post in 1982 on the "net.jokes" newsgroup
["Re: A child hood poem"](https://olduse.net/replies/24930-1354145783-1)

Eveline replied in 2013 to a post in 1983 on net.jokes.q
["Re: A couple"](https://olduse.net/replies/18258-1346155593-1)

> Ha!

Bill Leary replied in 2015 to Darin Johnson's post in 1985 on net.games.frp
["Re: frp & artwork"](https://olduse.net/replies/11119-1424043158-1)

Frederick Smith replied in 2021 to David Hoopes's post in 1990 on trial.rec.metalworking
["Re: Is this group still active?"](https://olduse.net/replies/4161133-1614850471-1)
