Reading `net.games`, we can see that many things have remained the same
across decades of time and the evolution of rogue into nethack.

Through posted high scores, we can see that
[team ant](http://article.olduse.net/254@Amhuxa.UUCP)
has always killed more than its share of young adventurers.

There are [tricky ways to cheat](http://article.olduse.net/1165@Autzoo.UUCP);
the bug is always already known to the shadowy dev team, but a fix not
yet released.

The post "[a rogue's tail](http://article.olduse.net/698@Aharpo.UUCP)"
is an early example of a style of storytelling often used in describing
ascention or other notable nethack games today.

> Tonite I  finally put it all together.  I was on level 15, I had
> a -1 ring of dexterity in my pack.  I entered a dark room and there in front
> of me was the nymph.  I quickly ran back into the tunnel.  Slowly I dropped
> each possession as I walked towards the temple that would bestow immorality
> upon me.  Saving my sword and armor least I be ambushed at the threshold I
> entered the room.  Two squares lay between me and the nymph.  I dropped my
> armor and stepped forward,  Soon I would know for sure.  I placed my mace
> carefully on the floor.   I stood naked but for the cursed ring.  Immortality?!
> I moved forward to attack the nymph.  CRASH!! I had fallen through a trapdoor...

And of course the few who have found the amulet of YENDOR and escaped to tell
the tale always lord it over the rest of us.

[[!meta author=Joey]]
