[A peek at the development of the 286](http://article.olduse.net/793@fortune.UUCP):

> As far as the 286 is concerned, as of this date, nobody's even seen a fully
> functional one.  The A-step samples out do not have the memory managment
> working yet and there are some pretty strange things happening with interrupts.
> And the darned thing won't run correctly with interrupts at 8 Mhz; you have
> to decrease the clock speed to about 7.5 Mhz.  Intel's due to sample the
> B-step parts later on this month.
