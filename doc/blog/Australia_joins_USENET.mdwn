Usenet is branching out internationally, with some sites in Europe already,
and now, [Australia_joins_USENET](http://article.olduse.net/467@sdchema.UUCP)

<pre>
        Data is actually collected at sdchema and transferred
        every week or so to a mag tape which is then sent airmail
        to the department of Computer Science at the University
        of Sydney in Australia where it is sent into the
        Australian network. It is a slow but cheap way of
        sending news, mail and files to Australian sites.
</pre>
