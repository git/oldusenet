to convert b-news articles:

- add an Xref header if none is present. For example:
  `Xref: wren.kitenet.net alt.callahans:275`
- if no Date header and there is a Posted header, copy it to Date
- dates do not need to be munged!
- if no Subject header, or it is empty or all whitespace, set to
  "Subject: -"
- if no Message-ID and there is an "Article-I.D." , copy it
- if Message-ID looks like "foo.125", change to "<125@foo.UUCP>"
  (transformation observed being made around end of 1983)
- if Message-ID lacks "@", add @unknown
- ensure Message-ID is all inside <...>
- add X-OldUsenet-Modified header collecting modifications done, if any..

to inject to leafnode:

- find all messages posted before current time 30 years ago, and inject each
- copy to next number in /var/spool/news/alt/newsgroup/name for each
  newsgroup in Newsgroups (avoid .. and / in names..)
- update /var/spool/news/leaf.node/groupinfo
  `alt.callahans 1300 3 0 ?`
  Here the 1300 is the current highest article number.
  Fields are: "$name $lastnum $firstnum $age $description"
  The age doesn't *seem* to matter... (but clients may care.. check)
- no leafnode hup needed
- test suite: run leafnode on full conversion and watch logs for it
  deleting corrupt messages

to convert a-news articles

From the 1st line, we have:

* Message-ID
* Newsgroups
* Path (also use as From, or take last part of bang path)
* Date
* Subject
* body (remainder)

Then pass through b-news converter.
