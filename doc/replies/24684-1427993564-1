Date: Thu, 02 Apr 2015 12:52:32 -0400
From: DMcCunney <dennis.mccunney@gmail.com>
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Thunderbird/31.5.0
MIME-Version: 1.0
Newsgroups: grc.scifi
Subject: Re: Worst SyFi Movie EVER!
References: <XnsA4698BB3EE93Dxs11eyahoocom@127.0.0.1> <030bhahm311j09bnqr1g1ic5q2tm72tud9@4ax.com> <mf42nh$2237$1@news.grc.com> <r6adhadf07envlgifdjhl2o6p45qgajo5c@4ax.com> <mf6uk2$11ct$1@news.grc.com> <XnsA46C9D5B164ABmikedmsncom@4.79.142.203> <mf9ser$27id$1@news.grc.com> <XnsA46D657CD2F3Dmikedmsncom@4.79.142.203> <mff4lv$1o67$1@news.grc.com> <XnsA46FDD18E1B80mikedmsncom@4.79.142.203>
In-Reply-To: <XnsA46FDD18E1B80mikedmsncom@4.79.142.203>
Content-Type: text/plain; charset=windows-1252; format=flowed
Content-Transfer-Encoding: 7bit
Path: kite.kitenet.net!not-for-mail
Message-ID: <sn80vb-c3o.ln1@kite.kitenet.net>

Mike D wrote:
> DMcCunney <dennis.mccunney@gmail.com> wrote in
> news:mff4lv$1o67$1@news.grc.com:
>
>> And lots of folks in Indiana are not thrilled by it.
>
> Yeah, I noticed that, happily <g>
>
>> But this is cultural.  The US has at least seven distinct regional
>> cultures, and local attitudes vary with them.
>
>> I grew up in Philadelphia.  Philadelphia still bears traces of Quaker
>> founder heritage.  For instance, the city has "Blue Laws" that
>> legislate what you may not do on Sunday.  One that is gradually
>> eroding is the prohibition against sales of alcohol on Sunday.  You
>> can now do things like buy beer in the supermarket. You didn't used to
>> be able to.
>
> IN still prohibits sales of caryout and bars on Sun.  They finally
> allowed sales in restaurants <whew> and they have been trying to get the
> No Sales ban lifted for years.  To be honest, as a former luquior store
> employee, I have to say that I don't mind the 'day off' <g>

I wouldn't either, but I'd assume that if I worked Sunday, I'd get a 
different day off.

>> Also, PA is one of the states where hard liquor is a state monopoly,
>> bought at a State Store.  Fortunately, at least some state outlets are
>> upgrading their stock.  But because it's a state monopoly, prices are
>> high due to state taxes, and there is substantial smuggling between NJ
>> and PA (and NJ and NY) in consequence.  Well, duh...
>
> Yes, Ohio (our next door neighbor) is the same.  I learned about that
> when I visited freinds there during my collage days <g>

And got asked to bring a few bottles bought at cheaper IN prices when 
you popped across the border?  :-)

>>> Sadly too many people want to 'protect' other people from what they
>>> don't like <sigh>  Unfortunately that seems not to be changing so
>>> much ... at least here :-/
>>
>> A former co-worker talked about attending a lecture by a California
>> law professor, who stated *all* such laws had an "us vs them" origin,
>> and were attempts by the dominant group to keep a thumb on a lesser
>> group.
>
> Sadly, that sounds about right.  Too many people believe that 'they know
> best for everyone else.

Partly "they know best" and partly "keep *them* in their *place!*"

>> Prohibition fell firmly into that category, as the previous wave of
>> immigrants who were agrarian and mostly Protestant looked askance at
>> the new wave of mostly Catholic immigrants flocking to the cities to
>> get factory work.
>
> Hmmm, I had always thought that it was the 'religeous zealots' that were
> the driving force.

Only in part.  Support broke down along ethnic, economic and geographic 
lines.  The religious zealots were in the Protestant camp.  The Catholic 
immigrants from places like Italy would not share those views.

One book I recently read was a history of Templeton Rye.  It was a 
prohibition product, named after the IA town where it was centered.  The 
founder, Joel Irlbeck, was the son of a Bavarian immigrant farmer in the 
area.  His goal was to produce a rye whiskey so good buyers would ask 
for it by name, and he succeeded.  He got the entire town of Templeton 
(population 428) in cahoots with him to evade Prohibition and make and 
sell rye whiskey, including the head of the bank and the monsignor of 
the local RC church.

Templeton, IA was *not* all that atypical, and clearly illustrated the 
class divide involved.

>> Criminalization of marijuana had similar origins, as pot was a favored
>> recreation in black communities.
>
> I am not sure that I buy that 'argumet' either.  That seems to smack of
> the same sort of 'profiling' that has police 'suspecting' blacks more
> than whites ... rightly or wrongly.  Blacks have always been ... 'less
> successful' in this country and so tended to be more likely to 'drown
> their sorrows' in what ever manner was available ... alcohol or other
> drugs.

It wasn't the only reason, but was a reason.  Remember that Cannabis is 
a weed that grew wild in many areas, and it *was* a favored recreation 
in black communities.  But not just blacks.  Decades back, my mother and 
I visited my paternal grandmother and great grandmother, who shared a 
house in North Philadelphia.  During our visit, an old male neighbor 
named Charlie, who looked in on my relatives and did occasional chores 
that required a stronger man stopped by to say hello.  I don't recall 
how, but the talk turned to marijuana.  Charlie said "Oh, yeah!  We used 
to smoke that stuff every day when I was a kid in Texas!"  My mother's 
face fell, as she and I had been having arguments on the matter.  I 
*really* wanted to say "Dude!  You and I need to get together and blow 
some righteous weed!", but discretion was the better part of valor. :-)

And racial profiling is a complex issue.

NYC went around that, as part of a concerted initiative to get illegal 
handguns off the streets.  Part of that initiative was a stop-and-frisk 
program.  It got a lot of opposition from black community activists who 
saw black people being humiliated and demeaned by it, as the efforts 
were concentrated in black communities.

Unfortunately, there was a reason for the concentration.  That's where 
the guns were.  The communities in question had gangs and drugs.  Gangs 
fought each other over turf, and there was an enormous amount of money 
in crack, meth, and smack, and the gangs were also involved in the drug 
trade.  When you have that kind of money, you get people willing to kill 
to get it, and kill to protect it.  There were good reasons from a 
gang-member's perspective to have and carry a gun.

There was a change in the city administration, and the stop-and-frisk 
program was cancelled.  The murder rate in those communities involving 
illegal handguns has soared.

A friend of a friend is a middle-aged black grandmother.  She is deeply 
unhappy about the cancellation of stop-and-frisk.  The lives in a decent 
neighborhood, but has friends and relatives living in affected areas who 
are now afraid to leave their homes after dark for fear of being caught 
in the crossfire of gang/drug related combat.

My question for the outraged community activists would be "I don't blame 
you for being upset, but how *do* the cops get illegal handguns off the 
street in your community?"

>> We're seeing similar things in NYC these days.  The City has been
>> boosting taxes on tobacco products, and a pack of brand name
>> cigarettes may retail for $14.  Part of it is a desire to get more tax
>> revenue, but part of it as a perception by the Powers that Be that
>> people *shouldn't* smoke.  They can't outright ban it, but they can
>> make it expensive and impose restrictions on where you can smoke.
>
> Yeah, lots of states and municipalities are trying that.  There is a
> point of dominishing returns, however <g>  There will always be those
> that will pay what it costs.  If it costs too much, they will find other
> ways to 'feed the need' <g>  Columbian tobacco lords <chuckle>

Electronic cigarettes are increasingly popular.

And smuggling will increase.  Upstate there is an Onondaga indian 
reservation. (http://www.onondaganation.org/) Drive onto the res, and 
the first thing you see is the 24/7 tobacco store.  The second thing you 
see is the modern community center/indoor lacrosse court built with 
profits from the tobacco store.

The reservation is not part of the State of New York.  It's a separate 
area created by treaty with the Federal Government and administered by 
the Bureau of Indian Affairs.  Because it's not part of NY State, the 
tobacco store does not charge state excise taxes.  Cigarettes are as low 
as $2.10 a pack for goods actually made in Canada.

The state has been trying to collect such taxes for ever, and the 
Onondaga essentially thumb their noses and say "You don't have 
jurisdiction.  We aren't *part* of New York State!"

The state can't do anything *on* the reservation, so they keep an eye on 
it.  If you drive in with a van, and drive out stuffed the the brim with 
cigarettes at reservation prices, you are likely to be intercepted on 
your way out once you are *in* NYS and busted for intent to resell 
unlicensed, untaxed products.

>> It got amusing, because first they dramatically increased taxes on
>> cigarettes, but loose tobacco had the old rate.  People suddenly
>> started rolling their own.  Then they boosted taxes on loose tobacco,
>> but cigarillos were still at the old rate.  The last time I looked,
>> you could still get those for about $2.50/pack in Brooklyn.
>
> Never underestimate the willingness of people to find a way around the
> government's attempt to take too much of their $$  <g>

I don't.  I had discussions elsewhere with a chap who lived in an areas 
where there were three different local tax jurisdictions within easy 
driving distance, and would plan his shopping to get things at the 
lowest applied tax rate.  He agreed that when you factored in his time, 
gas consumption, and wear on the car, it probably actually cost him 
*more* money, but he really, *really* didn't like taxes... :-)

>>> That is the problem I have with Macallan ... I have felt it was too
>>> pricey.
>>
>> The last time I bought the 15 year old sherried expression, I was
>> attending an SF con in MD.  I didn't have a chance to restock before I
>> left and decided to buy locally.  A liquor store down the street from
>> the hotel was having a sale, and I got a bottle for about 2/3rds of
>> what it would cost in NYC. Win!
>
> Nice find!!

It was a happy accident, but I knew MD had lower excise taxes than 
NYS/NYC.  I assumed it would be cheaper, but hadn't anticipated it would 
be *that* much cheaper.  ($50 there, $75 in NYC.)

>> I'm aware of the Scotch Malt Whiskey Society.  I own and moderate a
>> mailing list devoted to fine spirits, and list members post links like
>> that.  One close friend (and co-manager of the list) made a mid-life
>> career change.  His former employer had a dispute with the state of MA
>> over taxes, and closed the office he worked from.  He chose not to
>> relocate to TX to follow the employer, and morphed from computer
>> security specialist to representative of a French outfit that imported
>> Cognac, Armagnac, and a French single malt.  (The owners were old
>> friends.)  His income isn't back to prior levels, but he's having fun
>> and Boston is his sales territory, so there is lots of market to be
>> penetrated.
>
> Ooooo ... cognacs and armangnacs are another thing I cannot afford as
> often as would like <sigh>

Bill drank cognac before moving to whisky.  So did I.  Changing careers 
to spirits rep was a matter of "amateur turning pro".  He already had 
extensive knowledge (and single malts are a hobby for him, with a large 
home collection and books on the topic).  Having friends in the business 
made it easy for him to shift over.  (His employer also imports 
Abnsinthe, and serving *that* is a production number.)

>> When I get to Boston, one likely stop is Federal Wine and Spirits.
>> Their Spirits Manager, Joe Howell, is a world class whisk(e)y expert.
>> You tell Joe what your standard dram is, and he starts pulling things
>> off the shelves.  "You like X?  Maybe you'll like Y.  How about Z?"
>> He's generally dead on, and you will walk out happily lubricated even
>> if you *don't* buy. n His regular tastings are almost enough to make
>> me wish I lived in Boston. :-)
>
> I am certainly not the expert that Joe sounds like, but I used to do a
> lot of that and my favorite part was when a customer would come back in
> all excited about something I had suggested <g>

<grin>

Joe has been in the business for a long time, and is on a first name 
basis with master distillers and brand ambassadors.  Federal has won 
various "Best Whisk(e)y retailer" awards, and when brands are promoting 
new expressions in the US, Federal is one of the places they stop to do 
a dog and pony show and tasting.

I think Joe has probably forgotten more about whisky than most mere 
mortals will ever learn.

> MikeD
______
Dennis


