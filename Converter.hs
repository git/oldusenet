{- usenet message converter framework -}

module Converter where

import Distribution.Simple.Utils
import System.Environment
import System.FilePath
import Data.Time.Clock.POSIX
import System.IO
import Data.Digest.Pure.SHA
import qualified Data.ByteString.Lazy.UTF8 as L
import System.Directory
import Data.List
import Utility.Exception

import HashDir
import Msg
import MsgTime

deleteConverted :: Bool
deleteConverted = True

type Converter = Msg -> Maybe Msg
type Parser = String -> Maybe Msg

massConvert :: Parser -> Converter -> IO ()
massConvert parser converter = getArgs >>= mapM_ (recursiveConvert parser converter)

recursiveConvert :: Parser -> Converter -> FilePath -> IO ()
recursiveConvert parser converter dir = recurseDir (convert parser converter) dir

recurseDir :: (FilePath -> IO ()) -> FilePath -> IO ()
recurseDir a dir = do
	files <- getDirectoryContentsRecursive dir
	let files' = -- remove cruft
		filter (not . isPrefixOf "tape") $
		filter (not . isSuffixOf ".TARDIRPERMS.") files
	mapM_ a $ map (dir </>) files'

convert :: Parser -> Converter -> FilePath -> IO ()
convert parser converter file = do
	r <- tryIO $ convert' parser converter file
	case r of
		Left _ ->  warning $ "failed to load " ++ file
		Right v -> return v
convert' :: Parser -> Converter -> FilePath -> IO ()
convert' parser converter file = do
	c <- readFile file
	case parser c of
		Nothing -> warning $ "failed to parse " ++ file
		Just m -> case converter m of
			Nothing -> warning $ "failed to convert " ++ file
			Just m' -> do
				case extractTime m' of
					Left e -> warning $ file ++ ": " ++ e
					Right t -> do
						store m' t
						if deleteConverted
							then removeFile file
							else return ()

store :: Msg -> POSIXTime -> IO ()
store m t = do
	let content = assemble m
	let dir = "spool" </> dirHash t
	let filename = show $ sha1 $ L.fromString content
	createDirectoryIfMissing True dir
	writeFile (dir </> filename) content

warning :: String -> IO ()
warning = hPutStrLn stderr
