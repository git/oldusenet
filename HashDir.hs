{- date hashed directories
 -
 - Articles are stored in directories hashed based on seconds from epoch.
 - Example: 0307/33/67
 -
 - This is an efficient representation when the goal is to query for
 - articles posted before (or after) a given date.
 -}

module HashDir where

import System.Directory
import Distribution.Simple.Utils
import Data.Time.Clock.POSIX
import Data.String.Utils
import System.FilePath
import Data.Char

dirHash :: POSIXTime -> FilePath
dirHash = join "/" . dirSegment

dirSegment :: POSIXTime -> [String]
dirSegment t = segments padded
	where
		s = takeWhile isDigit (show t)
		-- zero pad to ten digets
		padded = take (10 - length s) padding ++ s
		padding = repeat '0'
		-- 3 directory levels; with tuned sizes for each;
		-- the last 2 digets are not included, so granularity
		-- is 99 seconds.
		segments l = [range l 0 4, range l 4 6, range l 6 8]
		range l start end = drop start $ take end l
		
{- To find articles posted before a given time, walk its dirSegments,
 - finding all files in directories that are less than the value of
 - the segment.
 -
 - For example, for 0307/33/67,
 - take all files in directories < 0307,
 - then all in directories < 0307/33, etc.
 -}
findBefore :: FilePath -> POSIXTime -> IO [FilePath]
findBefore dir t = walk (dirSegment t) dir []
	where
		walk [] _ c = return c
		walk (s:ss) d c = do
			found <- contents d s
			walk ss (d </> s) (found ++ c)
		contents top stop = do
			e <- doesDirectoryExist top
			subdirs <- if e 
				then getDirectoryContentsRecursive top
				else return []
			return $ map (top </>) $ filter (< stop) subdirs
