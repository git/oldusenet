{- usenet b-news to more or less modern usenet converter -}

import Data.String.Utils

import Msg
import Converter

main :: IO ()
main = massConvert aparse a2b

a2b :: Converter
a2b = Just
	. noteWasA
	. addFrom

noteWasA :: Step
noteWasA = note "converted from A-news"

{- A news did not have a From; use the last bang of the Path -}
addFrom :: Step
addFrom m = ensureHeader m (Header "From") $ last bangs
	where
		bangs = split "!" $ getHeader m (Header "Path")

aparse :: String -> Maybe Msg
aparse [] = Nothing
aparse s
	| 'A' == head s = aparse' $ lines s -- starts with "A"
	| otherwise = Nothing
aparse' :: [String] -> Maybe Msg
aparse' ls
	| length ls < 5 = Nothing -- too short
	| otherwise = Just $ Msg headers body
	where
		headers = zip keys ls
		body = unlines $ drop 5 ls
		keys = map Header [
			"Message-ID", -- including the "A" but that's ok
			"Newsgroups",
			"Path",
			"Date",
			"Subject"
			]
