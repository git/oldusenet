{- Leafnode active file. -}

module ActiveFile where

import Data.Time.Clock.POSIX
import qualified Data.Map as M
import System.Directory
import Data.Char
import Data.Maybe

import Leafnode

type ActiveMap = M.Map String Int

readActive :: IO ActiveMap
readActive = readFile activeFile >>= return . parseActive

parseActive :: String -> ActiveMap
parseActive = M.fromList . mapMaybe parseLine . lines

parseLine :: String -> Maybe (String, Int)
parseLine s
	| length ws > 1 = Just (ws !! 0, read $ ws !! 1)
	| otherwise = Nothing
	where
		ws = words s

writeActive :: ActiveMap -> IO ()
writeActive a = do
	now <- getPOSIXTime
	writeFile tmp $ assembleActive a now
	renameFile tmp activeFile
	where
		tmp = activeFile ++ ".new"

assembleActive :: ActiveMap -> POSIXTime -> String
assembleActive a time = unlines $ map assemble $ M.toList a
	where
		assemble (name, lastnum) =
			name ++ " " ++ show lastnum ++ " 1 " ++ epoch ++ " ?"
		epoch = takeWhile isDigit $ show time

nextMessage :: ActiveMap -> String -> (ActiveMap, Int)
nextMessage a newsgroup = (a', next)
	where
		current = M.lookup newsgroup a
		-- start at 2, because leafnode reserves message 1 for its
		-- placeholder messages
		next = maybe 2 (+ 1) current
		a' = M.insert newsgroup next a
