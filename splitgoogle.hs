-- Reads semi-mbox formatted file. These don't open right in mutt,
-- presumably the headers are messed up. Instead, look for
--
-- "From xxx"
-- X-Google-
--
-- Which indicates the top of a new message.
-- We assume that X-Google-* is highly unlikely to appear in usenet
-- messages. Why it would appear in these archives is an interesting
-- question..
--
-- Reads from stdin and creates a maildir in the current directory.

import qualified Data.ByteString.Lazy as B
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.Encoding as E
import Data.Char
import System.Directory

main :: IO ()
main = do
	createDirectory "maildir"
	createDirectory "maildir/cur"
	createDirectory "maildir/new"
	createDirectory "maildir/tmp"
	b <- B.getContents
	go (0 :: Integer) [] (B.split nl b)
  where
  	go _ [] [] = return ()
	go n c [] = emit n c
	go n c (l1:l2:ls) | from `B.isPrefixOf` l1 && xgoogle `B.isPrefixOf` l2 = do
		emit n c
		go (n+1) [l2,B.append l1 dummydate] ls
	go n c (l:ls) = go n (l:c) ls

	emit n l = do
		let f = "maildir/new/" ++ show n ++ ".msg"
		B.writeFile f (B.intercalate (B.pack [nl]) (reverse l))
	
	tobs = E.encodeUtf8 . T.pack

	from = tobs "From "
	xgoogle = tobs "X-Google-"
	nl = fromIntegral (ord '\n')
	
	-- Added to From line to improve mutt's display.
	dummydate = tobs "  Tue Nov 25 18:20:22 2014"
