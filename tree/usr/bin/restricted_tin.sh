#!/bin/sh
cleanup () {
	if [ -n "$t" ]; then
		rm -f $t
	fi
	exit
}
trap cleanup EXIT INT

# only needed when not using screen
max=$(cat /etc/oldusenet-shellbox-limit)
if [ "$(ps ax |grep re[s]tricted_tin | grep -v re[s]tricted_tin.sh | wc -l)" -gt $max ]; then
	echo "** Sorry, maximum web visitor limit of $max reached."
	echo "** Hit reload to try a mirror!"
	echo "** Or use a NNTP newsreader instead."
	logger "oldusenet max reached"
	#beer
	exit 1
fi

t=$(tempfile)
nice ionice -c3 sh -c \
	"LD_PRELOAD=/usr/lib/sudo/sudo_noexec.so \
	restricted_tin -r -x -g nntp.olduse.net -f \"$t\""
cleanup
