#!/bin/sh
cleanup () {
	exit
}
trap cleanup EXIT INT

# sometimes fails to connect, unknown why (race with another one
# under load? 
screen -xRR || screen -xRR || exit 1
