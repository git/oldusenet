{- recently posted messages -}

{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE BangPatterns #-}

module RecentMsg where

import qualified Data.Set as S
import Data.Time
import Data.Time.Clock.POSIX
import Data.Ord
import Data.List
import Data.Char
import System.Directory
import Text.Hamlet
import Text.Blaze.Html.Renderer.String
import Control.Applicative hiding (empty)
import Prelude

import qualified Msg
import qualified MsgTime

type MsgId = String
type RecentMsgs = S.Set RecentMsg

data RecentMsg = RecentMsg
	{ messageId :: String
	, date :: UTCTime
	, subject :: String
	, author :: String
	, newsgroups :: [String]
	} deriving (Eq, Show, Read)

instance Ord RecentMsg where
	compare = comparing date

load :: FilePath -> IO RecentMsgs
load f = do
	e <- doesFileExist f
	if e
		then do
			contents <- readFile f
			let contents' = length contents `seq` contents
			if null contents'
				then return empty
				else return $ read $ contents'
		else return empty

store :: FilePath -> RecentMsgs -> IO ()
store f m = writeFile f $ show m

insert :: FilePath -> Msg.Msg -> Int -> POSIXTime -> IO ()
insert f n limit time = do
	r <- load f
	let r' = addMsg r n limit time
	store f r'
	zone <- getCurrentTimeZone
	writeFile (f++".html") (genHtml r' time zone)

genHtml :: RecentMsgs -> POSIXTime -> TimeZone -> String
genHtml m time zone = renderHtml [shamlet|
!!!
<html>
  <head>
    <title>recent messages
    <link rel=stylesheet href=#{cssurl "style"}>
    <link rel=stylesheet href=#{cssurl "local"}>
  <body>
    <div>
      Recent messages at #{prettyTime zone time}:
      <ul style="list-style-type: none; margin: 0; padding: 6px .4em;">
        $forall message <- list
          <li>
            <a target=_parent href=#{articleurl message}>#{subject message}</a>
            <br>
            by #{email2name $ author message} to #{head $ newsgroups message}
|]
	where
		cssurl s = "http://olduse.net/" ++ s ++ ".css"
		articleurl message = "http://article.olduse.net/" ++
			(debracket $ messageId message)
		list = reverse $ sort $ S.elems m
		email2name = takeWhile (\c -> not (isSpace c) && c /= '@')
			. debracket
			. dropWhile isSpace
		debracket = filter (`notElem` "<>[]")

prettyTime :: TimeZone -> POSIXTime -> String
prettyTime zone = formatTime defaultTimeLocale "%l:%M %P %a %e %b %Y" .
	 utcToLocalTime zone . posixSecondsToUTCTime

empty :: RecentMsgs
empty = S.empty

fromMsg :: Msg.Msg -> POSIXTime -> RecentMsg
fromMsg n time = RecentMsg
	{ messageId = Msg.messageId n
	, date = posixSecondsToUTCTime $
		case MsgTime.extractTime n of
			Right t -> t
			Left _ -> time
	, subject = Msg.getHeader n (Msg.Header "Subject")
	, author = Msg.getHeader n (Msg.Header "From")
	, newsgroups = Msg.getNewsGroups n
	}

{- Adds a message to RecentMsgs, attempting to keep the size below a limit. -}
addMsg :: RecentMsgs -> Msg.Msg -> Int -> POSIXTime -> RecentMsgs
addMsg m n limit time = add m (fromMsg n time) limit

add :: RecentMsgs -> RecentMsg -> Int -> RecentMsgs
add m n limit
	| limit > size = go m
	| otherwise = go $ reduce m n
	where
		size = S.size m
		go = S.insert n

massAdd :: RecentMsgs -> [FilePath] -> Int -> IO RecentMsgs
massAdd m files limit = foldl' go m . sort <$> readall [] files
	where
		go x n = add x n limit
		readall c [] = return c
		readall c (f:fs) = do
			s <- readFile f
			case Msg.parse (length s `seq` s) of
				Nothing -> readall c fs
				Just msg -> do
					let !r = fromMsg msg 0
					readall (r:c) fs

{- Removes items from RecentMsgs, attempting to keep a diverse
 - mix of items that were all fairly recently posted.
 - Always removes at least one item. -}
reduce :: RecentMsgs -> RecentMsg -> RecentMsgs
reduce m n
	| S.size m == 0 = m
	| have sameMessageId = nuke sameMessageId
	| have tooOld = nuke tooOld
	| have sameSubjects = nuke sameSubjects
	| have similarSubjects = nuke similarSubjects
	| have sameNewsGroups = nuke sameNewsGroups
	| have sameAuthors = nuke sameAuthors
	| otherwise = nuke oldest
	where
		have = not . S.null
		first = head . sort . S.elems
		nuke x = S.delete (first x) m
		filterm a = S.filter a m
		filtersame f = filterm $ \x -> f x == f n
		sameMessageId = filtersame messageId
		sameSubjects = filtersame subject
		similarSubjects = filtersame (reducesubject . subject)
		sameAuthors = filtersame author
		sameNewsGroups = filterm $ \x -> not $ null $ intersect (newsgroups n) (newsgroups x)
		tooOld = filterm $ daysOlder n 2
		oldest = S.fromList [first m]
		reducesubject = unwords . filter (`notElem` relike) . words . map toLower
		relike = ["re", "re:", "fwd", "fwd:"]

daysOlder :: RecentMsg -> Integer -> RecentMsg -> Bool
daysOlder than days n = (utctDay $ date than) `diffDays` (utctDay $ date n) > days
