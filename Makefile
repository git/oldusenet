BINS=a2b b2c uucp splitgoogle

all:
	stack build
	for b in $(BINS); do \
		cp -a $$(find .stack-work/ -name $$b -type f | grep build/$$b/$$b | tail -n 1) $$b; \
	done

clean:
	stack clean
	rm -f $(BINS) *.o *.hi
	rm -rf .stack-work
