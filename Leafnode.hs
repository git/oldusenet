{- Leafnode locations. -}

module Leafnode where

import System.FilePath
import Data.String.Utils
import Data.List
import Utility.Path

activeFile :: FilePath
activeFile = spoolDir </> "leaf.node/groupinfo"

messageDir :: FilePath
messageDir = spoolDir </> "message.id"

newsGroupPath :: String -> FilePath
newsGroupPath newsgroup = foldl (</>) spoolDir (split "." newsgroup)

pathNewsGroup :: FilePath -> String
pathNewsGroup = intercalate "." . splitDirectories . relPathDirToFile spoolDir

spoolDir :: FilePath
spoolDir = "/var/spool/news"
