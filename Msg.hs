{- usenet message data type -}

module Msg where

import Data.Char
import Data.String.Utils
import Data.List
import Data.Maybe

type Step = Msg -> Msg

type AssocList a b = [(a, b)]

lookup1 :: Eq k => k -> AssocList k [e] -> [e]
lookup1 k = fromMaybe [] . lookup k

hasEntry :: Eq k => k -> AssocList k v -> Bool
hasEntry k = isJust . lookup k

{- An AssocList is used to preserve header order. -}
type Headers = AssocList Header Value

type Value = String
type Body = String

{- Make headers compare case-insensatively. -}
newtype Header = Header String
instance Eq Header where
	Header a == Header b = map toLower (show a) == map toLower (show b)
instance Show Header where
	show (Header h) = h

data Msg = Msg Headers Body deriving (Show)

{- Logs a modification to a message. -}
note :: String -> Step
note info m = replaceHeader m h $ old ++ sep ++ info
	where
		h = Header "X-OldUsenet-Modified"
		old = getHeader m h
		sep = if null old then "" else "; "

{- Checks if a header is present, and populated with non-whitespace -}
hasHeader :: Msg -> Header -> Bool
hasHeader msg = not . null . filter (not . isSpace) . getHeader msg

getHeader :: Msg -> Header -> Value
getHeader = onassoc lookup1

messageId :: Msg -> String
messageId m = getHeader m (Header "Message-Id")

onassoc :: (Header -> AssocList Header Value -> a) -> Msg -> Header -> a
onassoc a (Msg headers _) header = a header headers

{- Changes a header in the assoclist, preserving order or adding at end. -}
replaceHeader :: Msg -> Header -> Value -> Msg
replaceHeader (Msg headers body) header value =
	if hasEntry header headers
		then Msg (map rep headers) body
		else Msg (headers++[(header, value)]) body
	where
		rep i@(h, _)
			| h == header = (header, value)
			| otherwise = i
	
ensureHeader :: Msg -> Header -> Value -> Msg
ensureHeader m header dummyval
	| hasHeader m header = m
	| otherwise = note ("added " ++ show header) $
		replaceHeader m header dummyval

getNewsGroups :: Msg -> [String]
getNewsGroups m =
	nub $ map (map toLower) $ filter safe $ separate $ newsgroups
	where
		-- Should be separated by commas, but sometimes spaces were
		-- used, and very occasionally even other whitespace, 
		-- including newlines and tabs.
		separate s = concatMap splitWs $ split "," s
		safe s = '/' `notElem` s && not (null s)
		newsgroups = getHeader m $ Header "Newsgroups"

assemble :: Msg -> String
assemble (Msg headers body) = concatMap pp headers ++ "\n" ++ body
	where
		pp (h, v) = show h ++ ":" ++ sep v ++ "\n"
		sep v
			| null v = v
			| isSpace (head v) = v
			| otherwise = " " ++ v

{- basic rfc822 parser, very liberal in what it receives -}
parse :: String -> Maybe Msg
parse s
	| "A" `isPrefixOf` s = Nothing -- smells like A-news.
	| otherwise = parse' (lines s) []
parse' :: [String] -> Headers -> Maybe Msg
parse' [] [] = Nothing -- parse failed to find headers
parse' [] h = Just $ Msg h "" -- empty message
parse' (l:ls) h
	| null l = Just $ Msg (reverse h) (unlines ls) -- end of headers
	| otherwise = parseHeader l ls h

{- parses out the header, handling multiline headers -}
parseHeader :: String -> [String] -> Headers -> Maybe Msg
parseHeader c [] h = Just $ Msg (splitHeader c:h) "" -- empty body
parseHeader c rest@(l:ls) h
	| not (null l) && isSpace (l !! 0) = parseHeader (c++"\n"++l) ls h -- continued line
	| otherwise = parse' rest (splitHeader c:h) -- end of this header

splitHeader :: String -> (Header, Value)
splitHeader [] = error "empty header.. should never happen!"
splitHeader s
	| ':' `elem` s = (Header header, value)
	| otherwise = (Header s, "") -- preserve mangled header with no colon
	where
		bits = split ":" s
		header = bits !! 0
		value = dropWhile isSpace $ join ":" (tail bits)
