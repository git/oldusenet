{- Copys messages from spool into leafnode tree. Also handles extracting
 - message bundle tarballs at the right time, and removal of old groups
 - once all messages in them expire. -}

import System.Environment
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Time.Calendar
import System.FilePath
import System.Directory
import System.Posix.Files
import Control.Monad (unless)
import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.IfElse
import Utility.Exception
import Data.String.Utils
import Control.Applicative
import qualified Data.Map as M
import Prelude

import Leafnode
import ActiveFile
import Msg
import MsgTime
import qualified RecentMsg
import HashDir
import Utility.SafeCommand
import Utility.Path

main :: IO ()
main = do
	args <- getArgs
	if null args
		then do
			time <- getTime []
			putStrLn $ "next will be " ++ dirHash time ++ " ..."
		else do
			time <- getTime $ tail args
			process (args !! 0) time
			expireGroups

process :: FilePath -> POSIXTime -> IO ()
process incoming time = do
	extractTars incoming time
	a <- readActive
	todo <- findBefore incoming time
	unless (null todo) $ do
		-- sort todo, so that older messages are always added first
		a' <- move (sort todo) incoming a time
		writeActive a'

extractTars :: FilePath -> POSIXTime -> IO ()
extractTars incoming time =
	mapM_ untar =<< (filter newtar <$> getDirectoryContents incoming)
	where
		asnum s = read (takeWhile isDigit s) :: Int
		timestr = asnum $ show time
		newtar f = (".tar.gz" `isSuffixOf` f) && (asnum f <= timestr)
		untar f = do
			let f' = incoming </> f
			unlessM (boolSystem "tar"
				[ Params "xf", File f', 
				  Param "-C", File incoming ]) $
				error "tar failed"
			removeFile f'

move :: [FilePath] -> FilePath -> ActiveMap -> POSIXTime -> IO ActiveMap
move [] _ a _ = return a
move (x:xs) incoming a time = do
	-- move to leafnode's message dir
	let new = messageDir </> drop (length incoming + 1) x
	putStrLn new
	createDirectoryIfMissing True (parentDir new)
	msg <- betterFrom . fromJust . parse <$> readFile x
	installMessage msg new

	-- add into each newsgroup, and to the recentmsg file
	RecentMsg.insert (incoming </> "recentmsgs") msg 10 time
	a' <- addNewsGroup new a $ getNewsGroups msg
	
	removeFile x
	prune (parentDir x)
	-- todo clean up empty incoming dirs

	move xs incoming a' time

installMessage :: Msg -> FilePath -> IO ()
installMessage msg dest = writeFile dest $ assemble msg

{- Most conversions are done by a2b and b2c, but this allows doing more
 - without rebuilding the whole incoming spool. -}
betterFrom :: Step
betterFrom m
	| isbangpath = 
		note "fixed From" $ replaceHeader m fromHeader $
			last bangs ++ " <" ++ intercalate "!" bangs ++ ">"
	| otherwise = m
	where
		fromHeader = Header "From"
		from = getHeader m fromHeader
		bangs = split "!" from
		isbangpath = length bangs > 1 && not modernfrom
		modernfrom = any (== False) (map (\c -> notElem c from) "<> ")

prune :: FilePath -> IO ()
prune d
	| null d = return ()
	| otherwise = do
		r <- tryIO (removeDirectory d)
		case r of
			Left _ -> return () -- not empty
			Right _ -> prune (parentDir d)

addNewsGroup :: FilePath -> ActiveMap -> [String] -> IO ActiveMap
addNewsGroup _ a [] = return a
addNewsGroup src a (g:gs) = do
	let (a', num) = nextMessage a g
	let dest = newsGroupPath g </> show num

	{- There could be a message file with the same name as the news
	 - group. -}
	whenM (doesFileExist (parentDir dest)) $
		error $ "need " ++ parentDir dest ++ " to be a directory, but it's a file"
				
	createDirectoryIfMissing True (parentDir dest)
	{- Linking the message into place may fail, if the number of the
	 - message is the same as the name of an existing news group.
	 - This is an infelicity of leafnode's directory layout, and
	 - is worked around by just moving on to the next message number. -}
	catchIO (createLink src dest >> addNewsGroup src a' gs)
		(const $ addNewsGroup src a' (g:gs))

getTime :: [String] -> IO POSIXTime
getTime args
	| null args = do
		now <- getPOSIXTime
		return $ yearsAgo 30 now
	| otherwise =
		case parseTime $ args !! 0 of
			Left e -> error e
			Right t -> return t

{- This handles leap years right, so it's the same day of the month,
 - N years ago. -}
yearsAgo :: Integer -> POSIXTime -> POSIXTime
yearsAgo n = utcTimeToPOSIXSeconds . calc . posixSecondsToUTCTime
	where
		calc t = UTCTime 
			(addGregorianYearsRollOver (-1 * n) $ utctDay t)
			(utctDayTime t)

{- Expiry is done by a cron job that deletes old messages. This leaves
 - groups still in the ActiveMap, and still having .overview files.
 - Find directories in the spool that only contain .overview files 
 - or are fully empty and remove them, and remove from the ActiveMap. -}
expireGroups :: IO ()
expireGroups = do
	deleted <- go spoolDir
	m <- readActive
	let m' = foldl' (flip M.delete) m deleted
	writeActive m'
  where
	go d = do
		cs <- dirContents d
		l <- forM cs $ \c -> do
			isdir <- doesDirectoryExist c
			if isdir
				then do
					deleted <- go c
					return (Nothing, deleted)
				else return (Just c, [])
		let fs = catMaybes (map fst l)
		let deleted = concat (map snd l)
		if fs == [".overview"] || null fs
			then do
				mapM_ removeFile fs
				prune d
				return (pathNewsGroup d:deleted)
			else return deleted
